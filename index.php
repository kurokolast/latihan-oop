<?php
    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');
    
    $sheep = new Animal("shaun");
    echo "Name : ".$sheep->name."<br>";
    echo "legs : ".$sheep->legs."<br>"; 
    echo "cold_blooded : ".$sheep->cold_blooded."<br><br>";

    $Minifrog = new Frog("buduk");
    echo "Name : ".$Minifrog->name."<br>";
    echo "legs : ".$Minifrog->legs."<br>"; 
    echo "cold_blooded : ".$Minifrog->cold_blooded."<br>";
    echo $Minifrog->Jump()."<br><br>";

    $Miniape = new Ape("kera sakti");
    echo "Name : ".$Miniape->name."<br>";
    echo "legs : ".$Miniape->legs."<br>"; 
    echo "cold_blooded : ".$Miniape->cold_blooded."<br>";
    echo $Miniape->Yell();
?>